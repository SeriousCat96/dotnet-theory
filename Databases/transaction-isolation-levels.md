# Isolation-Problems

У параллельно выполняющихся транзакций могут возникать различные аномалии (проблемы), особенно если они работают с одним набором данных:

## Lost-update

Потерянное обновление - при одновременном изменении одного блока данных разными транзакциями теряются все изменения, кроме последнего. Возникает когда транзакции не изолированы друг от друга.

![](_attachments/Pasted%20image%2020240108013521.png)

## Dirty-Read

Грязное чтение – чтение данных, добавленных другой транзакцией, которая в последствии откатится.

![](_attachments/Pasted%20image%2020240108014427.png)

## Non-Repeatable-Read

Неповторяющееся чтение – при повторном чтении в рамках одной транзакции ранее прочитанные данные оказываются измененными.

![](_attachments/Pasted%20image%2020240108014626.png)

## Phantom-Read

Фантомное чтение – при повторном чтении в рамках одной транзакции одна и та же выборка даёт разные множества строк.

![](_attachments/Pasted%20image%2020240108015145.png)

Разница между [неповторяющимся чтением](#non-repeatable-read) и фантомным чтением в том, что в первом случае данные изменяются (UPDATE), а во втором добавляются/удаляются (CREATE/DELETE).

# Isolation-levels

Чтобы бороться с разными видами несогласованности данных при параллельном выполнении транзакций, требуется их [изолировать](ACID.md#isolation). 

**Уровень изоляции транзакции** – степень защиты внутренними механизмами СУБД от различного вида несогласованностей данных, возникающих при параллельном выполнении транзакций.

**Уровень изоляции транзакции** – условное значение, определяющее степень допустимости получения несогласованных данных при параллельном выполнении транзакций.

Стандарт SQL-92 выделяет 4 уровня изоляции в порядке её увеличения: [READ UNCOMMITTED](#Read-Uncommited), [READ COMMITTED](#Read-Commited), [REPEATABLE READ](#Repeatable-Read), [SERIALIZABLE](#Serializable). Каждый следующий уровень включает все предыдущие.

| Уровень изоляции | [Потерянное обновление](#lost-update) | [«Грязное» чтение](#dirty-read) | [Неповторяющееся чтение](#non-repeatable-read) | [Фантомное чтение](#phantom-read) |
| ---- | ---- | ---- | ---- | ---- |
| [READ UNCOMMITTED](#Read-Uncommited) | + | - | - | - |
| [READ COMMITTED](#Read-Commited) | + | + | - | - |
| [REPEATABLE READ](#Repeatable-Read) | + | + | + | - |
| [SERIALIZABLE](#Serializable) | + | + | + | + |

Однако чем выше уровень изоляции, тем меньше степень одновременного конкурентного доступа и больше накладных расходов (например, на поддержание блокировок), а значит ниже производительность.

## Read-Uncommited

**Read uncommitted** – это уровень изоляции, при котором каждая транзакция видит незафиксированные изменения другой транзакции.

Операции чтения других транзакций не изолируются, никаких блокировок чтения не накладывается. Считываемые данные могут быть несогласованными. Данный уровень допускает [грязное чтение](#dirty-read), [неповторяющееся чтение](#non-repeatable-read), [фантомное чтение](#phantom-read).

Применение данного уровня изоляции крайне не желательно и его следует применять только в тех случаях, когда точность данных не представляет важности или когда данные редко подвергаются изменениям.
## Read-Commited

**Read committed** – это уровень изоляции, при котором параллельно исполняющиеся транзакции видят только зафиксированные изменения других транзакций.
Накладывает [разделяемые блокировки](record-locking.md#shared-lock) на считываемые данные только на время выполнения инструкции чтения, предотвращая [чтение незафиксированных данных других транзакций](#dirty-read).
Данный уровень допускает [неповторяющееся чтение](#non-repeatable-read), [фантомное чтение](#phantom-read).
## Repeatable-Read

**Repeatable read** или snapshot isolation — это уровень изоляции, при котором транзакция не видит изменения данных, прочитанные ей ранее, однако способна прочитать новые данные, соответствующие условию поиска.
Накладывает [разделяемые блокировки](record-locking.md#shared-lock) на считываемые данные и удерживает их до завершения транзакции. А значит внутри транзакции эти данные не могут быть изменены. Но это не мешает другим инструкциям вставлять новые строки, которые могут попасть в последующие чтения.
Данный уровень допускает [фантомное чтение](#phantom-read).
## Serializable

**Serializable** — это уровень изоляции, при котором каждая транзакция выполняется так, как будто параллельных транзакций не существует. 
Данный уровень не допускает несогласованных состояний.