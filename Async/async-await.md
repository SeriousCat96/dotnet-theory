# Task

Основным механизмом паттерна TAP является класс `Task`. Чтобы выполнить задачу асинхронно, можно вызвать `Task.Run`, который выполняет задачу в потоке, взятом из [пула потоков](../Multithreading/processes-and-threads.md#ThreadPool). Можно выполнить задачу в отдельном потоке, с помощью `Task.Factory.StartNew` с опцией `TaskCreationOptions.LongRunning`.

# Как-работает-async-await

Асинхронные методы вызываются через оператор `await`. Такие методы помечаются ключевым словом `async`.

Оператор `await` приостанавливает выполнение текущего метода до завершения асинхронной операции, но не всегда. В случае применения `await` к уже завершенной задаче результат операции возвращается немедленно без приостановки выполнения метода в текущем потоке. 

То есть как бы говорит нам: 
>*"Если задача уже выполнена, дай мне её результат сразу и я пошёл дальше. Ещё нет результата? Тогда выполни задачу и верни результат, я пока займусь другими делами. Когда выполнишь, я вернусь и доделаю работу."*

Рассмотрим работу async-await на следующем примере:
```csharp
public class AwaitOperator
{
    public static async Task Main()
    {
        Task<int> downloading = DownloadDocsMainPageAsync();
        Console.WriteLine($"{nameof(Main)}: Launched downloading.");

        int bytesLoaded = await downloading;
        Console.WriteLine($"{nameof(Main)}: Downloaded {bytesLoaded} bytes.");
    }

    private static async Task<int> DownloadDocsMainPageAsync()
    {
        Console.WriteLine($"{nameof(DownloadDocsMainPageAsync)}: About to start downloading.");

        var client = new HttpClient();
        byte[] content = await client.GetByteArrayAsync("https://learn.microsoft.com/en-us/");

        Console.WriteLine($"{nameof(DownloadDocsMainPageAsync)}: Finished downloading.");
        return content.Length;
    }
}
// Output similar to:
// DownloadDocsMainPageAsync: About to start downloading.
// Main: Launched downloading.
// DownloadDocsMainPageAsync: Finished downloading.
// Main: Downloaded 27700 bytes.
```

В данном примере весь код до вызова `await client.GetByteArrayAsync("https://learn.microsoft.com/en-us/");` выполняется синхронно. Далее выполнение метода `DownloadDocsMainPageAsync` приостанавливается, управление сразу возвращается методу `Main`. Метод `Main` выполняется синхронно до тех пор, пока ему не потребуется результат асинхронной операции, выполняемой методом `DownloadDocsMainPageAsync`. И вот при вызове `await downloading` мы запускаем выполнение задачи. Когда `client.GetByteArrayAsync` получает все байты, вычисляется остальная часть метода `DownloadDocsMainPageAsync`. После этого вычисляется остальная часть метода `Main`.

На самом деле, если совсем кратко, то компилятор создаёт и возвращает задачу с типом `Task<int>`, внутри которой содержится собственно вызов `client.GetByteArrayAsync("https://learn.microsoft.com/en-us/")`, создаёт callback, куда помещает  остальную часть метода `DownloadDocsMainPageAsync`, которая будет вызвана после завершения данной задачи, плюс запоминает текущий [контекст синхронизации](#Контекст-синхронизации), и записывает её в переменную `downloading`.

# Контекст-синхронизации

Класс `SynchronizationContext` предназначен для исполнения кода в потоке конкретного вида. 

`SynchronizationContext.Current` позволяет получить контекст для текущего потока.

В классе `SynchronizationContext` есть важный метод `Post`, который гарантирует, что переданный делегат будет исполняться в правильном контексте.

При вызове оператора `await` сохраняется текущий контекст синхронизации. Когда метод возобновляется, компилятор вставляет вызов `Post`, чтобы исполнение происходило в запомненном контексте. 

>Восстановление контекста вызывает определенные накладные расходы. Если производительность стоит на первом месте или речь идет о библиотечном коде, которому безразлично, в каком потоке выполняться, то, возможно, не имеет смысла нести такие расходы. Поэтому, в таком случае следует сконфигурировать задачу с помощью метода `Task.ConfigureAwait` и передать параметру `continueOnCapturedContext` значение `false`. Тогда при возобновлении исполнения не будет вызываться метод `Post` запомненного контекста SynchronizationContext

В .NET имеются разные реализации контекста синхронизации:
* WinForms, WPF - инкапсулируют единственный поток - поток пользовательского интерфейса.
* **ASP .NET** - никак не изменяет потоки, служит для корректного заполнения статических полей типа `HttpContext.Current`.
* **ASP .NET Core** - инкапсулирует потоки из [пула потоков](../Multithreading/processes-and-threads.md#ThreadPool). 
* **Консольное приложение** - контекст синхронизации отсутствует.

Метод может возобновиться в потоке, отличном от того, где был начат, при выполнении одного из условий:
* если запомненный контекст `SynchronizationContext` инкапсулирует несколько потоков, например [пул потоков](../Multithreading/processes-and-threads.md#ThreadPool); 
* если контекст не подразумевает переключения потоков; 
* если в точке, где встретился оператор `await`, вообще не было текущего контекста синхронизации, как, например, в консольном приложении; 
* если объект `Task` сконфигурирован так, что при возобновлении `SynchronizationContext` не используется (например через `Task.ConfigureAwait`).

То есть использовать `Task.ConfigureAwait` в консольном приложении или ASP .NET Core приложении не имеет смысла.
# В-каком-потоке-работает-мой-код

>Часто бывает, что выражение в строке, содержащей первый `await`, содержит еще один async-метод. Поскольку это выражение предшествует первому `await`, оно также выполняется в вызывающем потоке. Таким образом, вызывающий поток продолжает «углубляться» в код приложения, пока не встретит метод, действительно возвращающий объект `Task`.

```csharp
internal class Program
{
	static async Task Main(string[] args)
	{
		Console.WriteLine($"{nameof(Main)}: starting RunAsync. ThreadId={Environment.CurrentManagedThreadId}");
		await RunAsync();
		Console.WriteLine($"{nameof(Main)}: RunAsync finished. ThreadId={Environment.CurrentManagedThreadId}");
	}

	static async Task RunAsync()
	{
		Console.WriteLine($"{nameof(RunAsync)}: starting InternalRunAsync. ThreadId={Environment.CurrentManagedThreadId}");
		await InternalRunAsync();
		Console.WriteLine($"{nameof(RunAsync)}: InternalRunAsync finished. ThreadId={Environment.CurrentManagedThreadId}");
	}

	static async Task InternalRunAsync()
	{
		Console.WriteLine($"{nameof(InternalRunAsync)}: starting AsyncOperation. ThreadId={Environment.CurrentManagedThreadId}");
		await Task.Run(() => Console.WriteLine($"AsyncOperation: processing real async operation. ThreadId={Environment.CurrentManagedThreadId}"));
		Console.WriteLine($"{nameof(InternalRunAsync)}: AsyncOperation finished. ThreadId={Environment.CurrentManagedThreadId}");
	}
}

// Output similar to:
// Main: starting RunAsync. ThreadId=1        
// RunAsync: starting InternalRunAsync. ThreadId=1
// InternalRunAsync: starting AsyncOperation. ThreadId=1
// AsyncOperation: processing real async operation. ThreadId=9
// InternalRunAsync: AsyncOperation finished. ThreadId=6
// RunAsync: InternalRunAsync finished. ThreadId=6
// Main: RunAsync finished. ThreadId=6
```

>`async` не планирует выполнение метода в фоновом потоке. Единственный способ сделать это – воспользоваться методом `Task.Run`, который специально предназначен для этой цели, или чем-то подобным. 

То есть `async` и `await` не гарантируют нам, что задача будет выполнена в другом потоке.

Выражение `await` не блокирует поток, в котором оно выполняется. Вместо этого оно указывает компилятору объявить оставшуюся часть асинхронного метода как продолжение ожидаемой задачи. Управление затем возвращается методу, вызвавшему асинхронный метод. Когда задача завершается, она вызывает свое продолжение и возобновляет выполнение асинхронного метода с того места, где она была прервана.

То есть до первой реально асинхронной операции код будет выполняться в вызывающем потоке. А дальше всё зависит от реализации [контекста синхронизации](#Контекст-синхронизации) и настроек объекта `Task` (использование `ConfigureAwait`).

![](_attachments/Pasted%20image%2020231229051940.png)
1. Пользователь нажимает кнопку, обработчик `GetButton_OnClick` помещается в очередь.
2. Поток пользовательского интерфейса исполняет первую половину метода `GetButton_OnClick`, включая вызов `GetFaviconAsync`.
3. Поток пользовательского интерфейса входит в метод `GetFaviconAsync` и исполняет его первую половину, включая вызов `DownloadDataTaskAsync`. 
4. Поток пользовательского интерфейса входит в метод `DownloadDataTaskAsync`, который начинает скачивание и возвращает объект `Task`. 
5. Поток пользовательского интерфейса покидает метод `DownloadDataTaskAsync` и доходит до оператора `await` в методе `GetFaviconAsync`. 
6. Запоминается текущий контекст `SynchronizationContext` – поток пользовательского интерфейса. 
7. Метод `GetFaviconAsync` приостанавливается оператором `await`, и задача `Task` из `DownloadDataTaskAsync` извещается о том, что она должна возобновиться по завершении скачивания (в запомненном контексте `SynchronizationContext`). 
8. Поток пользовательского интерфейса покидает метод `GetFaviconAsync`, который вернул объект `Task`, и доходит до оператора `await` в методе `GetButton_OnClick`. 
9. Как и в предыдущем случае, оператор `await` приостанавливает метод `GetButton_OnClick`.
10. Поток пользовательского интерфейса покидает метод `GetButton_OnClick` и освобождается для обработки других действий пользователя (*В этот момент мы ждем, пока скачается значок. На это может уйти несколько секунд. Отметим, что поток пользовательского интерфейса свободен и может обрабатывать другие действия пользователя, а порт завершения ввода-вывода пока не задействован. Во время исполнения операции ни один поток не заблокирован*).
11. Скачивание завершается, и порт завершения ввода-вывода ставит в очередь метод `DownloadDataTaskAsync` для обработки полученного результата. 
12. Поток порта завершения ввода-вывода помечает, что задача `Task`, возвращенная методом `DownloadDataTaskAsync`, завершена. 
13. Поток порта завершения ввода-вывода исполняет код обработки завершения внутри `Task`; этот код вызывает метод `Post` запомненного контекста `SynchronizationContext` (поток пользовательского интерфейса) для продолжения. 
14. Поток порта завершения ввода-вывода освобождается для обработки других событий ввода-вывода. 
15. Поток пользовательского интерфейса находит команду, отправленную методом `Post`, и возобновляет исполнение второй половины метода `GetFaviconAsync` – до конца. 
16. Покидая метод `GetFaviconAsync`, поток пользовательского интерфейса помечает, что задача `Task`, возвращенная методом `GetFaviconAsync`, завершена. 
17. Поскольку на этот раз текущий контекст синхронизации совпадает с запомненным, вызывать метод `Post` не нужно, и поток пользовательского интерфейса продолжает работать синхронно.
18. Поток пользовательского интерфейса возобновляет исполнение второй половины метода `GetButton_OnClick` – до конца.