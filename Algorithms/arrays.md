# Бинарный поиск

Вычислительная сложность: `O(log(n))`  
Выделение дополнительной памяти: - `O(1)` через цикл, `O(log(n))` через рекурсию  
  
Бинарный поиск — это метод поиска в упорядоченном массиве, при котором алгоритм ищет элементы в ограниченной области поиска, причем с каждым шагом область поиска делится на две части.

![](_attachments/Binary-search-work.gif)

Преимущества метода:
- Быстро (`O(log(n)`)  
Недостатки:
- Массив всегда должен быть упорядоченным
- Некоторые данные нельзя упорядочить (например географические координаты), на них бинарный поиск работать не будет.

Псевдокод с помощью цикла:
```pseudocode
function binary_search(A, count, key) is
    left := 0
    right := n − 1
    while left ≤ right do
        // для избежания переполнения вычисляем left + (right - left)
        mid := left + floor((right - left) / 2)
        if A[mid] < key then
            left := mid + 1
        else if A[mid] > key then
            right := mid − 1
        else:
            return mid
    return -1
```
Псевдокод с помощью рекурсии:
```pseudocode
function binary_search(A, count, key) is
    return binary_search(A, 0, count - 1, key) 

function binary_search(A, left, right, key) is
    if left > right then
        return -1
    mid := left + floor((right - left) / 2)
    if A[mid] < key then
        return binary_search(A, mid + 1, right, key)
    if A[mid] > key then
        return binary_search(A, left, mid - 1, key)
    return mid
```

Пример кода с помощью цикла:
```csharp
public int Search(int[] array, int key)
{
	int left = 0;
	int right = array.Length - 1;
	while (left <= right)
	{
		int mid = left + (right - left) / 2;
		if (array[mid] < key)
		{
			left = mid + 1;
		}
		else if (array[mid] > key)
		{
			right = mid - 1;
		}
		else
		{
			return mid;
		}
	}
	
	return -1;
}
```

Пример кода с помощью рекурсии:
```csharp
public int Search(int[] array, int key)
{
	return SearchRecursive(array, 0, array.Length - 1, key);
}

private int Search(int[] array, int left, int right, int key)
{
	if (left > right) return - 1;
	int mid = left + (right - left) / 2;
	if (array[mid] < key)
	{
		return Search(array, mid + 1, right, key);
	}
	if (array[mid] > key)
	{
		return Search(array, left, mid - 1, key);
	}
	return mid;
}
```


