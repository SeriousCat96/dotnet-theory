
# Потокобезопасность

Код потокобезопасный, если он исправно работает одновременно в нескольких [потоках](processes-and-threads.md#Потоки). В частности должен быть обеспечен правильный доступ к разделяемым ресурсам. Если код даёт непредсказуемый результат при одновременном выполнении в нескольких потоках - код не является потокобезопасным.

# Синхронизация-потоков

Для обеспечения [потокобезопасности](#Потокобезопасность) при доступе к разделяемым ресурсам должна выполняться синхронизация [потоков](processes-and-threads.md#потоки).
## Атомарные-операции

Атомарные операция - операция, которая выполняется как единое целое. Никакая другая операция не может повлиять на неё и не будет выполнена, пока выполняется текущая операция.

Примеры атомарных операций:
* чтение или запись переменных типа `bool`, `char`, `byte`, `sbyte`, `short`, `ushort`, `uint`, `int`, `float`.
* чтение или запись переменной перечисления (`enum`), использующих один из перечисленных выше типов в качестве базового.
* чтение или запись переменной [ссылочного типа](../MemoryManagement/value-reference-types.md#Reference-Types).

Примеры неатомарных операций:
* инкремент/декремент (состоит из 3 операций: чтение, сложение, запись).

## Interlocked

Класс `Interlocked` предоставляет [атомарные операции](#Атомарные-операции) для переменных, используемых в нескольких [потоках](processes-and-threads.md#потоки). Эти операции будут выполняться дольше, но они [потокобезопасны](#Потокобезопасность).

* Инкремент/декремент:
```csharp
int i = 0;
/*...*/
Interlocked.Increment(ref i); // Атомарно увеличивает i на 1. 
Interlocked.Decrement(ref i); // Атомарно уменьшает i на 1. 
```

* Exchange (меняет значение переменной на другое и возвращает предыдущее значение):
```csharp
int i = 0;
var x = Interlocked.Exchange(ref i, 5); // i == 5, x == 0 
```

* CompareExchange (меняет значение переменной на другое при совпадении значения с указанным и возвращает предыдущее значение этой переменной):
```csharp
int i = 0;

// в переменную i запишет 1, если там 0 и вернет в х предыдущее значение i.
var x = Interlocked.CompareExchange(ref i, 1, 0); // i == 1, x == 0
x = Interlocked.CompareExchange(ref i, 5, 0); // i == 1, x == 1
```

## lock

*Критическая секция* - секция в которой может одновременно выполняться не более одного [потока](processes-and-threads.md#потоки).

Реализовано на уровне языка с помощью оператора `lock`. Представляет собой синтаксический сахар над механизмом синхронизации [Monitor](#Monitor). Если сомневаемся какой примитив синхронизации выбрать, в 99% случаев `lock` решит задачу.

Объект, который передается в `lock` является идентификатором критической секции. Если поток захватывает этот объект, то другой поток не сможет попасть в критическую секцию (при чем не обязательно в том же участке кода).

```csharp
object syncRoot = new object();
/*...*/
lock (syncRoot)
{
	// критическая секция. Только 1 поток, захвативший syncRoot сможет сюда попасть.
}
```

Рекомендуется создавать поле с типом `object` внутри класса, как в примере выше.

Не рекомендуется использовать `lock(this)` или `lock("abc")`, чтобы избежать вмешательства извне (в другом участке программы можно вызвать `lock` с тем же объектом или строкой и повлиять на результат).
## Monitor

Высокоуровневый механизм синхронизации, допускающий в [критическую секцию](#lock)
не более 1 [потока](processes-and-threads.md#потоки).

```csharp
lock (syncRoot)
{
	// ...
}
```
разворачивается в
```csharp
Monitor.Enter(syncRoot);
try
{
	// ...
}
finally
{
	Monitor.Exit(syncRoot);
}
```

Методы:
* `Monitor.Enter` - входит в критическую секцию, получая блокировку для переданного объекта.
* `Monitor.Exit` - снимает блокировку объекта и выходит из критической секции.

Следующие методы можно вызвать только внутри критической секции:
* `Monitor.Wait` - снимает блокировку объекта, разрешая другому потоку войти в критическую секцию, и переводит поток в режим ожидания, до получения особого сигнала.
* `Monitor.Pulse` - посылает сигнал об изменении состояния блокированного объекта и пробуждает один ожидающий поток. Потоки пробуждаются в том же порядке, в котором засыпали. Если в момент сигнала не было ожидающих потоков, то сигнал игнорируется.
* `Monitor.PulseAll` - то же самое, что `Pulse`, только пробуждает все ожидающие потоки.

Допускается из одного потока несколько раз захватить один и тот же объект. Количество входов в критическую секцию должно совпадать с количеством выходов:
```csharp
Monitor.Enter(syncRoot);
Monitor.Enter(syncRoot);
try
{
	// ...
}
finally
{
	Monitor.Exit(syncRoot);
	Monitor.Exit(syncRoot);
}
```

## SpinLock

Высокоуровневый механизм синхронизации, аналогия [монитора](#monitor), отличается тем, что в отличии от монитора это [структура](../MemoryManagement/value-reference-types.md#Value-types), что позволяет снизить накладные расходы на [сборку мусора](../MemoryManagement/garbage-collector.md). 

Данная структура полезна при большом количестве блокировок (например, для каждого элемента в списке) и чрезмерно коротких периодов их удержания. При передаче экземпляров SpinLock следует соблюдать осторожность. Из-за того, что SpinLock определена как [struct](../MemoryManagement/value-reference-types.md#Value-types), ее присваивание приводит к созданию копии. Поэтому экземпляры SpinLock должны всегда передаваться по ссылке.

Методы:
* `Enter` - входит в критическую секцию, получая блокировку для переданного объекта.
* `Exit` - снимает блокировку объекта и выходит из критической секции.

Кроме того, SpinLock предлагает свойства для предоставления информации в случае, если в текущий момент находится в заблокированном состоянии: `IsHeld` и `IsHeldByCurrentThread`.

## WaitHandle 

Абстрактный базовый класс, инкапсулирующий специфичные для операционной системы примитивы синхронизации.

Наследники: [Mutex](#mutex), [Semaphore](#semaphore), [EventWaitHandle](#eventwaithandle).

Полезные методы класса:
* [WaitOne](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.waithandle.waitone?view=net-7.0#system-threading-waithandle-waitone) –  Блокирует текущий поток до получения сигнала
* [WaitAll](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.waithandle.waitall?view=net-8.0#system-threading-waithandle-waitall) – Ожидает получения сигнала всеми элементами указанного массива. Есть перегрузки с таймаутами. Максимальное число элементов массива – 64.
* [WaitAny](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.waithandle.waitany?view=net-8.0) – Ожидает получения сигнала хотя бы от одного элемента указанного массива. Есть перегрузки с таймаутами. Возвращает индекс элемента массива, получившего сигнал или значение [WaitTimeout](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.waithandle.waittimeout?view=net-8.0#system-threading-waithandle-waittimeout), если ни один из элементов не получил сигнал при указанном таймауте.  Максимальное число элементов массива – 64.

## Mutex

[**Mutex**](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.mutex?view=net-7.0) (*mutual exclution – взаимное исключение или мьютекс*) – примитив синхронизации, обеспечивающий доступ в [критическую секцию](#lock) для не более одного [потока](processes-and-threads.md#потоки). Мьютекс предназначен для тех ситуаций, в которых общий ресурс может быть одновременно использован только в одном потоке.  Если поток получает мьютекс, второй поток, который хочет получить этот мьютекс, приостанавливается до тех пор, пока первый поток не освобождает мьютекс. 

Механизм [Monitor](#monitor) под капотом использует мьютекс.

Мьютексы бывают: 
* **локальные**: без имени, существуют только в текущем [процессе](processes-and-threads.md#процессы), его может использовать любой [поток](processes-and-threads.md#потоки) в процессе.
* **системные**: имеют имя, видны всей операционной системе . Такой мьютекс можно использовать для синхронизации между процессами (например, для реализации Single-Instance приложения можно использовать именованный мьютекс).

В пример мьютекса из жизни - **защёлка**. Человек заходит в уборную, запирает дверь защелкой – теперь никто кроме него не может попасть в уборную. Уборная – это критическая секция, люди – это потоки, защелка – мьютекс.

Методы:
* [OpenExisting](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.mutex.openexisting?view=net-7.0#system-threading-mutex-openexisting(system-string)) – открывает указанный именованный системный мьютекс, если он уже существует.
* [TryOpenExisting](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.mutex.tryopenexisting?view=net-7.0#system-threading-mutex-tryopenexisting(system-string-system-threading-mutex@)) – открывает указанный именованный системный мьютекс, если он уже существует, и возвращает значение, указывающее, успешно ли выполнена операция.
* [ReleaseMutex](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.mutex.releasemutex?view=net-7.0#system-threading-mutex-releasemutex) – освобождает мьютекс

## Semaphore

[**Semaphore**](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.semaphore?view=net-7.0) (семафор) – примитив синхронизации, в основе которого лежит счётчик, который ограничивает доступ [потокам](processes-and-threads.md#потоки) в [критическую секцию](#lock). Счётчик [атомарно](#Атомарные-операции) уменьшается, когда очередной поток заходит в критическую секцию и атомарно увеличивается, когда выходит.  Если счётчик равен нулю, то последующие потоки блокируются, пока другие потоки не отпустит семафор.

![](_attachments/Pasted%20image%2020231115013408.png)

Можно сказать, что [мьютекс](#mutex) – это двоичный семафор, у которого счетчик может принимать значения 0 и 1.
Аналогично мьютексам, семафоры бывают локальные и системные (если задать имя).

Есть облегченная версия [SemaphoreSlim](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.semaphoreslim?view=net-7.0), которая не использует семафоры операционной системы. Рекомендуется использовать для синхронизации в одном [процессе](processes-and-threads.md#процессы).

Методы:

* [OpenExisting](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.mutex.openexisting?view=net-7.0#system-threading-mutex-openexisting(system-string)) – открывает указанный именованный системный семафор, если он уже существует.
* [TryOpenExisting](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.mutex.tryopenexisting?view=net-7.0#system-threading-mutex-tryopenexisting(system-string-system-threading-mutex@)) – открывает указанный именованный системный семафор, если он уже существует, и возвращает значение, указывающее, успешно ли выполнена операция.
* [Release](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.semaphore.release?view=net-7.0#system-threading-semaphore-release) – Выходит из семафора один или несколько раз и возвращает последнее значение счетчика.

## EventWaitHandle

Представляет собой событие синхронизации потоков. В конструктор передается флаг – наличие сигнального состояния: `true` при наличии, `false` для несигнального состояния.
Сигнальное состояние означает, что выполнение вызывающего [потока](processes-and-threads.md#Потоки) не приостанавливается.

Методы:
* [Set](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.eventwaithandle.set?view=net-7.0#system-threading-eventwaithandle-set) – Задаёт сигнальное состояние события, позволяя одному или нескольким потокам продолжить выполнение.
* [Reset](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.eventwaithandle.reset?view=net-7.0#system-threading-eventwaithandle-reset) – Задаёт несигнальное состояние события, вызывая блокирование потоков.

Имеет две реализации: [AutoResetEvent](#autoresetevent) и [ManualResetEvent](#manualresetevent), которые отличаются лишь способом установки в несигнальное состояние.

### AutoResetEvent

Реализация [EventWaitHandle](#EventWaitHandle), в которой после установки сигнального состояния методом `Set` объект автоматически переходит в несигнальное состояние, позволяя одному потоку продолжить выполнение.

Работает по принципу **турникета**, пропускающего по одному человеку.

![](_attachments/Pasted%20image%2020231119020503.png)
### ManualResetEvent

Реализация [EventWaitHandle](#EventWaitHandle), в которой после установки сигнального состояния методом `Set` объект не переходит в несигнальное состояние, позволяя всем потокам продолжить выполнение. Несигнальное состояние устанавливается вручную методом `Reset`.

Работает как **дверь**: открывая дверь, ее необходимо закрыть, чтобы больше никто не смог пройти.

![](_attachments/Pasted%20image%2020231119020514.png)

Есть облегченная версия [ManualResetEventSlim](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.manualreseteventslim?view=net-8.0), которая не использует события синхронизации операционной системы.

## ReaderWriterLockSlim

Механизм синхронизации, позволяющий одновременно получать доступ к ресурсу множеству считывающих процессов (readers), но только одному записывающему (writer). Многочисленные считывающие процессы могут получать доступ к ресурсу только в том случае, если его не блокирует никакой записывающий процесс, а блокировать ресурс разрешено только одному записывающему процессу.

Методы:
* [EnterReadLock](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.readerwriterlockslim.enterreadlock?view=net-7.0#system-threading-readerwriterlockslim-enterreadlock) – пытается выполнить вход в блокировку в режиме чтения
* [TryEnterReadLock](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.readerwriterlockslim.tryenterreadlock?view=net-7.0#system-threading-readerwriterlockslim-tryenterreadlock(system-int32)) – пытается войти в блокировку в режиме чтения с необязательным указанием времени ожидания целым числом. Возвращает значение, указывающее, успешно ли поток вошел в режим чтения
* [EnterWriteLock](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.readerwriterlockslim.enterwritelock?view=net-7.0#system-threading-readerwriterlockslim-enterwritelock) – пытается выполнить вход в блокировку в режиме записи
* [TryEnterWriteLock](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.readerwriterlockslim.tryenterwritelock?view=net-7.0#system-threading-readerwriterlockslim-tryenterwritelock(system-int32)) – пытается войти в блокировку в режиме записи с необязательным указанием времени ожидания целым числом. Возвращает значение, указывающее, успешно ли поток вошел в режим чтения

Если задача сначала выполняет чтение и лишь затем запись, она может получать обновляемую блокировку чтения с помощью [EnterUpgradeableReadLock](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.readerwriterlockslim.enterupgradeablereadlock?view=net-7.0#system-threading-readerwriterlockslim-enterupgradeablereadlock) и [TryEnterUpgradeableReadLock](https://learn.microsoft.com/ru-ru/dotnet/api/system.threading.readerwriterlockslim.tryenterupgradeablereadlock?view=net-7.0#system-threading-readerwriterlockslim-tryenterupgradeablereadlock(system-int32)). В этом случае блокировка записи может быть получена и без снятия блокировки чтения.