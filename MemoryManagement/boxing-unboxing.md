
Упаковка (boxing) – это когда мы объект с типом значения "упаковываем" в объект ссылочного типа. То есть выполняем копирование данных объекта из [стека](memory-management.md#Stack) в [управляемую кучу](memory-management.md#Heap).

```csharp
int i = 123;
object o = i;
```
![](_attachments/20231112230531.png)


Обратная операция, когда мы перемещаем данные из управляемой кучи в стек, называется распаковкой (unboxing). При этом еще необходимо удостовериться, что тип упакованного объекта соответствует целевому.

```csharp
int i = 123;
object o = i;
int j = (int)o;
```
![](_attachments/20231112230737.png)

boxing/unboxing - не бесплатные операции, их лучше стараться избегать. При распаковке можно наткнуться на `System.InvalidCastException` при несоответствии типов.